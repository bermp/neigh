class User < ActiveRecord::Base
	has_many :brays, dependent: :destroy
	validates :password, confirmation: true
	validates_uniqueness_of :username, :email
	validates_presence_of :username, :email, :lname, :fname
	validates :password, presence: true, on: :create
	validates :password_confirmation, presence: true, on: :create
	
end