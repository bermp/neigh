class BraysController < ApplicationController

	before_action :authenticate_user!, except: [:index, :show]
	def index
		@brays = Bray.all
	end

	def new
		@bray = Bray.new
	end

	def edit
	end

	def show
		@bray = Bray.find(params[:id])
	end

	def create
		@bray = Bray.new(params.require(:bray).permit(:body).merge(user:current_user))

		if @bray.save
			redirect_to root_path, notice: "New bray"
		else
			render :new
	end

	def update
	end

	def destroy
		@bray = Bray.find(params[:id])
		@bray.destroy
		redirect_to root_path, alert: "Bray deleted"
	end

end