class CreateBrays < ActiveRecord::Migration
	def change
	  	create_table :brays do |t|
	  		t.text :body
	  		t.references :user, index: true
	  		t.timestamps null: false
 		end
 		add_foreign_key :brays, :users
	end
end