require 'sinatra'
require 'sinatra/activerecord'
#require 'bundler/setup' #loads gemfile defined ver of rack-flash
require 'rack-flash'
require './models'


set :database, "sqlite3:neigh.sqlite3"

# Current user sesssion
set :sessions, true
#enable :sessions

use Rack::Flash, sweep: true

def current_user
	session[:user_id] ? User.find(session[:user_id]) : nil
end

get '/' do
	erb :home
end

post '/login' do
	my_user = User.find_by email: params[:email]
	if my_user and my_user.password == params[:password]
		session[:user_id] = my_user.id
		redirect to ('/user')
	else
		redirect to ('/signup')
	end
end

get '/user' do
	erb :user
end

get '/signup' do
	erb :signup
end

post '/signup' do
	if 	session[:current_user_id] != nil
		redirect to ('/user')
	else
		new_user = User.create(fname: params[:fname], lname: params[:lname], email: params[:email], password: params[:password])
		session[:user_id] = new_user.id
		flash[:notice] = "New account created"
		redirect to ('/user')
	end
end

get '/logout' do
	session[:user_id] = nil
	flash[:notice] = "You have logged out"
	redirect to ('/')
end