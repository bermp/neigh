Rails.application.routes.draw do

	get '/login', to: 'sessions#new'
	post 'login', to: 'sessions#create'
	delete '/logout', to: 'sessions#destroy'

	
  resources :users, except: [:index, :edit, :show]
  get '/profile/edit', to: 'user#edit', as: 'edit_profile'
  get '/profile/close', to: 'user#close', as: 'close_profile'
  resources :brays
  root 'brays#index'
end
